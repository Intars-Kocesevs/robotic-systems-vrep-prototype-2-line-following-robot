function sysCall_threadmain()
    sim.setThreadSwitchTiming(2)
    
    -- Put some initialization code here
    cameraUpDown = sim.getObjectHandle("noveroshanasKamera_UpDown")
    cameraLeftRight = sim.getObjectHandle("noveroshanasKamera_LeftRight")
    cameraDevice = sim.getObjectHandle("noverKamera_ierice")
    debugingInfo = sim.auxiliaryConsoleOpen("debugInfo", 8, 1)
    -- Put your main loop here, e.g.:
    --
       
    sim.auxiliaryConsolePrint(debugingInfo, 
    "Robots brauks pa trasi, neietrieksies sienaas un reages uz zaljo krasu.\n")
    
    while (sim.getSimulationState()~=sim.simulation_advancing_abouttostop) do
              
        testaSignalsPieslegumam, datuPaka1, datuPaka2 = sim.readVisionSensor(cameraDevice)
        if (datuPaka2) then
            blobCount = datuPaka2[1]   -- trekoto objektu skaits
            dataSizePerBlob = datuPaka2[2]
            for i = 1, blobCount, 1 do
                blobSize = datuPaka2[2 + (i - 1)*dataSizePerBlob + 1]
                blobOrientation = datuPaka2[2 + (i - 1)*dataSizePerBlob + 2]
                blobPosition = {datuPaka2[2 + (i - 1)*dataSizePerBlob + 3],  datuPaka2[2 + (i -1)*dataSizePerBlob + 4]}
                blobBoxDimensions = {datuPaka2[2 + (i - 1)*dataSizePerBlob + 5], datuPaka2[2 + (i - 1)*dataSizePerBlob + 6]}
            
            -- viss augstakais bez manuala un developeru pousta forumaa diez vai ienaktu praataa
            sim.auxiliaryConsolePrint(debugingInfo,string.format("\nx: %0.2f,  y: %0.2f", blobPosition[1], blobPosition[2]))
            --sim.setJointTargetVelocity(cameraUpDown, 10*(0.5 - blobPosition[1])*math.pi/180)
            --sim.setJointTargetVelocity(cameraLeftRight, 10*(0.5 - blobPosition[2])*math.pi/180)
            sim.setJointTargetVelocity(cameraUpDown, (0.5 - blobPosition[2]))
            sim.setJointTargetVelocity(cameraLeftRight, 1.5*(0.5 - blobPosition[1]))
            end
    
    --     local p=sim.getObjectPosition(objHandle,-1)
    --     p[1]=p[1]+0.001
    --     sim.setObjectPosition(objHandle,-1,p)
         --sim.switchThread() -- resume in next simulation step
        end
    end
    
end

function sysCall_cleanup()
    -- Put some clean-up code here
end


-- ADDITIONAL DETAILS:
-- -------------------------------------------------------------------------
-- If you wish to synchronize a threaded loop with each simulation pass,
-- enable the explicit thread switching with 
--
-- sim.setThreadAutomaticSwitch(false)
--
-- then use
--
-- sim.switchThread()
--
-- When you want to resume execution in next simulation step (i.e. at t=t+dt)
--
-- sim.switchThread() can also be used normally, in order to not waste too much
-- computation time in a given simulation step
-- -------------------------------------------------------------------------