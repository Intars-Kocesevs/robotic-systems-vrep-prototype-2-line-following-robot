function sysCall_init()
         
    -- This is executed exactly once, the first time this script is executed
    bubbleRobsBase=sim.getObjectAssociatedWithScript(sim.handle_self) -- this is bubbleRob's handle
    leftMotor=sim.getObjectHandle("bubbleRobLeftMotor") -- Handle of the left motor
    rightMotor=sim.getObjectHandle("bubbleRobsRightMotor") -- Handle of the right motor
    noseSensor=sim.getObjectHandle("bubbleRobsa_sensorDeguns") -- Handle of the proximity sensor
    minMaxSpeed={50*math.pi/180,300*math.pi/180} -- Min and max speeds for each motor
    backUntilTime=-1 -- Tells whether bubbleRob is in forward or backward mode
    -- Zemak izveidojam piesaistes pie tris video sensoru objektiem
    floorSensorHandles = {-1, -1, -1}
    floorSensorHandles[1] = sim.getObjectHandle("leftSensor")
    floorSensorHandles[2] = sim.getObjectHandle("centerSensor")
    floorSensorHandles[3] = sim.getObjectHandle("rightSensor")

    ----------------------------
    -- Create the custom UI:
    xml = '<ui title="'..sim.getObjectName(bubbleRobsBase)..' speed" closeable="false" resizeable="false" activate="false">'..[[
                <hslider minimum="0" maximum="100" on-change="speedChange_callback" id="1"/>
            <label text="" style="* {margin-left: 300px;}"/>
        </ui>
        ]]
    ui=simUI.create(xml)
    speed=(minMaxSpeed[1]+minMaxSpeed[2])*0.5
    simUI.setSliderValue(ui,1,100*(speed-minMaxSpeed[1])/(minMaxSpeed[2]-minMaxSpeed[1]))
    
end

function speedChange_callback(ui,id,newVal)
    speed=minMaxSpeed[1]+(minMaxSpeed[2]-minMaxSpeed[1])*newVal/100
end


function sysCall_actuation() 
    result=sim.readProximitySensor(noseSensor) -- Read the proximity sensor
    -- If we detected something, we set the backward mode:
    if (result>0) then backUntilTime=sim.getSimulationTime()+4 end 

    -- uzsakam darbu ar sekoshanu linijai; sagatavojam vajadzigo
    sensorReading = {false, false, false}
    for i = 1,3,1 do
        result,data = sim.readVisionSensor(floorSensorHandles[i])
       if (result >= 0) then
           sensorReading[i] = (data[11]<0.3)
       end
       print(sensorReading[i])
    end
    
    -- rekinam kreisa un labaa motora apgriezienus, lai sekot linijai:
    rightVelocity = speed
    leftVelocity = speed
    if sensorReading[1] then
        leftVelocity = 0.03 * speed
    end
    if sensorReading[3] then
    	rightVelocity = 0.03 * speed
    end
    if sensorReading[1] and sensorReading[3] then
    	backUntilTime = sim.getSimulationTime() + 2
    end


    if (backUntilTime<sim.getSimulationTime()) then
        -- When in forward mode, we simply move forward at the desired speed
        sim.setJointTargetVelocity(leftMotor,leftVelocity)
        sim.setJointTargetVelocity(rightMotor,rightVelocity)
    else
        -- When in backward mode, we simply backup in a curve at reduced speed
        sim.setJointTargetVelocity(leftMotor,-speed/2)
        sim.setJointTargetVelocity(rightMotor,-speed/8)
    end
end

function sysCall_cleanup() 
    simUI.destroy(ui)
end 