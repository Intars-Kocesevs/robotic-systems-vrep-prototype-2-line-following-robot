# robotic systems - VREP prototype 2 - line following robot

Experimental study of a virtual robot prototyping



## Description
These are two exploratory V-REP scenes, made to work out and test basics of a line-following algorithms for moving robot as well as robot
camera's tracking and color perception task.

## Installation
To open a .ttt scene files one needs to have installed V-REP (now in newer versions called CoppeliaSim). Scene files has been created in
V-REP PRO EDU, Version 3.5.0 (rev. 4) 64-bit. Lua script files can be opened and explored in any word processing utility.

## Usage
Two separate scenes allow to see moving and line-following as well green color objects tracking robot. Open scene in V-REP and launch
simulation to follow process in real-time.

## Support
kocesevs.intars@gmail.com

## Roadmap
Further idea is of scenes with differential height and more than one color type recognition. Currently, Lua script files and scene objects
have one downside - some of the script variable and some of the scene object's names are in non-English (native) naming - that is to be
changed to English-only in next update of this project.

## License
Licensed under the Apache License, Version 2.0

## Project status
This project will be followed by next; scenes from this project may be of value to explore, get ideas and impression of their basic
implementation in V-REP scene from which to build upon more elaborate scenes.
